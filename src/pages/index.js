import GetStarted from './GetStarted';
import Splash from './Splash';
import Signup from './Signup';
import Signin from './Signin';
import UploadPhoto from './UploadPhoto';
import Doctor from './Doctor';
import Messages from './Messages';
import Hospitals from './Hospitals';
import ChooseDoctor from './ChooseDoctor';
import Chatting from './Chatting';
import UserProfile from './UserProfile';
import UpdateUserProfile from './UpdateUserProfile';
import DoctorProfile from './DoctorProfile';

export {  
    GetStarted, 
    Splash, 
    Signup, 
    Signin, 
    UploadPhoto, 
    Doctor, 
    Messages, 
    Hospitals, 
    ChooseDoctor, 
    Chatting,
    UserProfile,
    UpdateUserProfile,
    DoctorProfile,
};
