import React, {useState, useEffect} from 'react';
import { ScrollView, View } from 'react-native';
import { Button, Gap, Header, Input, Profile } from '../../components';
import { getData, storeData, messageSuccess, messageError } from '../../utils';
import { firebase } from '../../config';
import { launchImageLibrary } from 'react-native-image-picker';
import styles from './styles.js';

const UpdateUserProfile = ({navigation}) => {
    const [isLoading, setIsLoading] = useState(false);
    const [user, setUser] = useState({
        uid: '',
        fullName: '',
        job: '',
        email: '',
        photo: '',
    })
    const [password, setPassword] = useState('');
    useEffect(() => {
        getData('user').then((response) => {
            setUser(response);
        })
    }, [])
    const onInputChange = (key, value) => {
        setUser({
            ...user,
            [key]: value
        })
    }
    const getImage = () => {
        launchImageLibrary({includeBase64: true, maxHeight: 210, maxWidth: 210}, (response) => {
            if(response.didCancel) {
                messageError('Photo gagal dipilih!!');
            }else{
                setUser({
                    ...user,
                    photo: `data:${response.type};base64, ${response.base64}`
                })
            }
        })
    }
    const updateProfile = () => {
        const data = {...user, photo: user.photo};
        setIsLoading(true);
        firebase.database()
            .ref(`users/${user.uid}/`)
            .update(data)
            .then(() => {
                setIsLoading(false);
                messageSuccess('Profile Berhasil disimpan!');
                storeData('user', data);
                setTimeout(() => {
                    navigation.replace('MainApp');
                }, 1500);
            })
            .catch(() => {
                setIsLoading(false);
                messageError('Profile Gagal disimpan!');
            })
    }
    const updatePassword = () => {
        setIsLoading(true);
        firebase.auth()
            .onAuthStateChanged((response) => {
                if(response) {
                    response.updatePassword(password).catch((error) => {
                        setIsLoading(false);
                        messageError(error.message);
                    })
                }
            })
    }
    const updateUserProfile = () => {
        if(password.length > 0) {
            if(password.length < 5) {
                messageError('Password minimal 6 karakter!');
            }else{
                updateProfile();
                updatePassword();
            }
        }else{
            updateProfile();
        }
    }
    return (
        <View style={styles.container}>
            <Header title="Edit Profile" onPress={() => navigation.goBack()} />
            <ScrollView showsVerticalScrollIndicator={false}>
                {
                    user.fullName.length > 0 && (
                        <Profile 
                            photo={user.photo} 
                            onPress={getImage}
                            remove
                        />
                    )
                }
                <View style={styles.form}>
                    <Input label="Full Name" value={user.fullName} onChangeText={(value) => onInputChange('fullName', value)} />
                    <Gap height={24}/>
                    <Input label="Pekerjaan"  value={user.job} onChangeText={(value) => onInputChange('job', value)} />
                    <Gap height={24}/>
                    <Input label="Email Address" value={user.email} disable />
                    <Gap height={24}/>
                    <Input label="Password" value={password} onChangeText={(value) => setPassword(value)} secureTextEntry />
                    <Gap height={40}/>
                    <Button isLoading={isLoading} onPress={updateUserProfile} title="Save Profile" />
                </View>
            </ScrollView>
        </View>
    )
}

export default UpdateUserProfile;
