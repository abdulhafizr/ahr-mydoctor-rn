import { StyleSheet } from 'react-native';
import { colors } from '../../utils';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    form: {
        padding: 40,
        paddingTop: 0,
        marginTop: 10,
    },
})

export default styles;
