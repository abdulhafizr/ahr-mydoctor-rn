import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../utils';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingHorizontal: 40,
		paddingTop: 40,
		paddingBottom: 44,
		justifyContent: 'space-between',
	},
	caption: {
		color: colors.white,
		fontSize: 28,
		lineHeight: 33.6,
		marginTop: 91,
		fontFamily: fonts.primary[200],
	},
});

export default styles;
