import React from 'react';
import { ImageBackground, View, Text, ScrollView } from 'react-native';
import { Button, Gap } from '../../components';
import { ILGetStarted, ILLogo } from '../../assets';
import styles from './styles';

const GetStarted = ({navigation}) => {
    return (
		<ImageBackground source={ILGetStarted} style={styles.container}>
			<View style={styles.content}>
				<ILLogo />
				<Text style={styles.caption}>Konsultasi dengan dokter jadi lebih mudah & fleksibel</Text>
			</View>
			<View>
				<Button title="Get Started" onPress={() => navigation.navigate('Signup')} />
				<Gap height={16} />
				<Button title="Sign In" type="secondary" onPress={() => navigation.navigate('Signin')} />
			</View>
		</ImageBackground>
    );
}

export default GetStarted;
