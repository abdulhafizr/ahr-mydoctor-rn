import { StyleSheet } from 'react-native';
import { colors } from '../../utils';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    content: {
        marginTop: 10,
    },
    buttonWrapper: {
        padding: 40,
        paddingTop: 23,
    }
})

export default styles;
