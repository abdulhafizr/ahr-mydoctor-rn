import React from 'react';
import { ScrollView, View } from 'react-native';
import { Button, DoctorDetail, Gap, Header, Profile } from '../../components';
import styles from './styles.js';

const DoctorProfile = ({navigation, route}) => {
    const {fullName, profession, photo, university, hospital_address, str_number, uid} = route.params;
    return (
        <View style={styles.container}>
            <Header title="Profile Dokter" onPress={() => navigation.goBack()} />
            <ScrollView>
                <Profile fullName={fullName} job={profession} photo={photo} />
                <View style={styles.content}>
                    <DoctorDetail label="Alumnus" value={university} />
                    <Gap height={16} />
                    <DoctorDetail label="Tempat Praktik" value={hospital_address} />
                    <Gap height={16} />
                    <DoctorDetail label="No. STR" value={str_number} />
                </View>
                <View style={styles.buttonWrapper}>
                    <Button 
                        title="Start Consultation" 
                        onPress={() => navigation.navigate('Chatting', {
                            fullName,
                            profession,
                            photo,
                            uid,
                        })} 
                    />
                </View>
            </ScrollView>
        </View>
    )
}

export default DoctorProfile;
