import React, {useState, useEffect} from 'react';
import { View, ScrollView } from 'react-native';
import { Header, List } from '../../components';
import { firebase } from '../../config';
import styles from './styles.js';

const ChooseDoctor = ({navigation, route}) => {
    const category = route.params;
    const [listDoctor, setListDoctor] = useState([]);
    useEffect(() => {
        firebase.database()
            .ref('doctors')
            .orderByChild('category')
            .equalTo(category)
            .once('value')
            .then((response) => {
                const doctors = response.val();
                if(doctors) {
                    const data = [];
                    Object.keys(doctors).map((key) => {
                        data.push({
                            id: key,
                            data: doctors[key],
                        })
                    })
                    setListDoctor(data);
                }
            })
    }, [])
    return (
        <View style={styles.container}>
            <Header title={`Pilih ${category}`} type="choose doctor" onPress={() => navigation.goBack()} />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.content}>
                    {
                        listDoctor.map((doctor) => (
                            <List 
                                onPress={() => navigation.navigate('DoctorProfile', {...doctor.data})} 
                                image={doctor.data.photo}
                                name={doctor.data.fullName} 
                                submenu={doctor.data.gender} 
                                rightArrow  
                            />
                        ))
                    }
                </View>
            </ScrollView>
        </View>
    )
}

export default ChooseDoctor;
