import React, {useState, useEffect} from 'react';
import { ScrollView, View } from 'react-native';
import { Header, Profile, List } from '../../components';
import { getData, messageSuccess, messageError } from '../../utils';
import { firebase } from '../../config';
import styles from './styles.js';

const UserProfile = ({navigation}) => {
    const [user, setUser] = useState({
        fullName: '',
        job: '',
        photo: '',
    })
    useEffect(() => {
        navigation.addListener('focus', () => {
            getData('user').then((response) => {;
                setUser(response);
            })
        })
    }, [navigation])
    const signOut = () => {
        firebase.auth().signOut()
            .then(() => {
                messageSuccess('Signout Success!');
                navigation.replace('GetStarted');
            })
            .catch((error) => {
                messageError(error.message);
            })
    }
    return (
        <View style={styles.container}>
            <Header title="Profile" onPress={() => navigation.goBack()} />
            <ScrollView showsVerticalScrollIndicator={false}>
                
                {
                    user.fullName.length > 0 && <Profile fullName={user.fullName} job={user.job} photo={user.photo} />
                }
                    
                <List 
                    icon="edit-profile" 
                    name="Edit Profile" 
                    submenu="Last updated yesterday" 
                    onPress={() => navigation.navigate('UpdateUserProfile')}
                    rightArrow 
                />
                <List 
                    icon="language" 
                    name="Language" 
                    submenu="Available 12 languages" 
                    rightArrow 
                />
                <List 
                    icon="give-use-rate" 
                    name="Give Us Rate" 
                    submenu="On Google Play Store" 
                    rightArrow 
                />
                <List 
                    icon="help-center" 
                    name="Signout" 
                    submenu="Read our guidelines" 
                    rightArrow 
                    onPress={signOut}
                />
            </ScrollView>
        </View>
    )
}

export default UserProfile;
