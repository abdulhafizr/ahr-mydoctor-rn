import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../utils';

const styles = StyleSheet.create({
	container: {
		paddingHorizontal: 40,
		paddingTop: 40,
		paddingBottom: 64,
		flex: 1,
		justifyContent: 'space-between'
	},
	caption: {
		fontSize: 20,
		color: colors.text.primary,
		marginTop: 40,
		fontFamily: fonts.primary[200],
	},
	content: {
		marginTop: 40,
	},
});

export default styles;