import React, {useState} from 'react';
import { View, Text, ScrollView } from 'react-native';
import { Button, Input, Link, Gap } from '../../components';
import { ILLogo } from '../../assets';
import { firebase } from '../../config';
import { storeData, messageError } from '../../utils';
import styles from './styles';

const Signin = ({navigation}) => {
	const [form, setForm] = useState({
		email: '',
		password: '',
		disableButton: true,
	})
	const [isLoading, setIsLoading] = useState(false);
	const onInputChange = (key, value) => {
		setForm({
			...form,
			[key]: value,
			disableButton: (form.email.length > 0 && form.email.includes('@') && form.password.length >= 5) ? false : true
		})
	}
	const onSignin = () => {
		setIsLoading(true);
		const {email, password} = form;
		firebase.auth()
			.signInWithEmailAndPassword(email, password)
			.then((response) => {
				firebase.database()
					.ref(`users/${response.user.uid}`)
					.once('value')
					.then((responseDB) => {
						setIsLoading(false);
						storeData('user', responseDB);
						navigation.replace('MainApp');
					})
			})
			.catch(({message}) => {
				setIsLoading(false);
				if(message === 'There is no user record corresponding to this identifier. The user may have been deleted.') {
					message = 'Email dan Password tidak ditemukan!';
				}else if(message === 'The password is invalid or the user does not have a password.') {
					message = 'Password salah!';
				}
				messageError(message);
			})
	}
    return (
		<ScrollView>
			<View style={styles.container}>
				<View>
					<ILLogo />
					<Text style={styles.caption}>Masuk dan mulai berkonsultasi</Text>
				</View>
				<View style={styles.content}>
					<Input label="Email Address" value={form.email} onChangeText={(value) => onInputChange('email', value)} />
					<Gap height={24} />
					<Input 
						label="Password" 
						value={form.password}
						onChangeText={(value) => onInputChange('password', value)}
						secureTextEntry
					/>
					<Gap height={10} />
					<Link label="Forgot My Password" size={12} />
					<Gap height={40} />
					{
						form.disableButton ? <Button disable title="Signin" /> : <Button title="Signin" isLoading={isLoading} onPress={onSignin} />
					}
					<Gap height={30} />
					<Link label="Create New Account" align="center" onPress={() => navigation.navigate('Signup')} />
				</View>
			</View>
		</ScrollView>
    );
}

export default Signin;
