import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../utils';

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.primary,
        flex: 1,
    },
    content: {
        backgroundColor: colors.white,
        flex: 1,
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
        overflow: 'hidden',
    },
    title: {
        marginTop: 30,
        marginLeft: 16,
        fontSize: 20,
        fontFamily: fonts.primary[600],
        color: colors.primary,
    },
});

export default styles;
