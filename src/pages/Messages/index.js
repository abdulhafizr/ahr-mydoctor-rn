import React, {useState, useEffect} from 'react';
import { ScrollView, Text, View } from 'react-native';
import { List } from '../../components';
import { getData } from '../../utils';
import { firebase } from '../../config';
import styles from './styles.js';

const Messages = ({navigation}) => {
    const [user, setUser] = useState({});
    const [historyChat, setHistoryChat] = useState([]);
    
    useEffect(() => {
        getUserFromLocal();

        const rootDB = firebase.database().ref();
        const historyMessages = rootDB.child(`messages/${user.uid}`);
        
        historyMessages.on('value', async (snapshot) => {
            const allHistoryChat = snapshot.val();
                if(allHistoryChat) {
                    const data = [];
                    const promises = await Object.keys(allHistoryChat).map(async (key) => {
                        const detailDoctor = await rootDB.child(`doctors/${allHistoryChat[key].uidPartner}`).once('value');
                        data.push({
                            id: key,
                            detailDoctor: detailDoctor.val(),
                            ...allHistoryChat[key],
                        })
                    })
                    await Promise.all(promises);
                    setHistoryChat(data);
                }
            })

    }, [user.uid]);
    const getUserFromLocal = () => {
        getData('user').then((response) => {
            setUser(response);
        })
    }
    return (
        <View style={styles.container}>
                <View style={styles.content}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <Text style={styles.title}>Messages</Text>
                        <View>
                            {
                                historyChat.map((chat) => (
                                    <List 
                                        key={chat.id}
                                        name={chat.detailDoctor.fullName} 
                                        submenu={chat.lastChat}
                                        image={chat.detailDoctor.photo} 
                                        onPress={() => navigation.navigate('Chatting', {...chat.detailDoctor})}
                                    />
                                ))
                            }
                        </View>
                    </ScrollView>
                </View>
        </View>
    )
}

export default Messages

