import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../utils';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    date: {
        fontSize: 11,
        fontFamily: fonts.primary[400],
        color: colors.gray1,
        textAlign: 'center',
        paddingVertical: 20,
    },
    chatBody: {
        flex: 1,
    },
});

export default styles;
