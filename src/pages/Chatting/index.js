import React, {useState, useEffect} from 'react';
import { ScrollView, Text, View } from 'react-native';
import { BubbleChat, ChatInput, Header } from '../../components';
import { firebase } from '../../config';
import { getData } from '../../utils';
import styles from './styles.js';

const Chatting = ({navigation, route}) => {
    const {fullName : doctorName, profession, photo : doctorPhoto, uid : doctorUid} = route.params;
    const [user, setUser] = useState({});
    const [chatFromFirebase, setChatFromFirebase] = useState([]);
    useEffect(() => {
        getUserFromLocalStorage();
        
        firebase.database()
            .ref(`chatting/${user.uid}_${doctorUid}/allChat`)
            .on('value', (snapshot) => {
                if(snapshot.val()) {
                    const snapshotChat = snapshot.val();
                    const allChat = [];
                    
                    Object.keys(snapshotChat).map((key1) => {
                        const dayChat = [];
                        Object.keys(snapshotChat[key1]).map((key2) => {
                            dayChat.push({
                                id: key2,
                                data: snapshotChat[key1][key2],
                            })
                        })
                        allChat.push({
                            date: key1,
                            data: dayChat,
                        })
                    })
                    setChatFromFirebase(allChat);
                }
            })
    }, [user.uid, doctorUid])
    
    const getUserFromLocalStorage = () => {
        getData('user').then((response) => {
            setUser(response);
        })
    }
    return (
        <View style={styles.container}>
            <Header title={doctorName} profession={profession} photo={doctorPhoto} type="chat header" onPress={() => navigation.goBack()} />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.chatBody}>
                    {
                        chatFromFirebase.map((date) => {
                            return (
                                <>
                                    <Text key={date.date} style={styles.date}>{date.date}</Text>
                                    {
                                        date.data.map((chat) => (
                                            <BubbleChat
                                                key={chat.id} 
                                                isDoctor={!(user.uid === chat.data.sentBy)}
                                                message={chat.data.message} 
                                                timestamp={chat.data.chatTime}
                                                photo={doctorPhoto}
                                            />
                                        ))
                                    }
                                </>
                            )
                        })
                    }
                </View>
            </ScrollView>
            <ChatInput 
                doctorUid={doctorUid} 
                doctorName={doctorName}
            />
        </View>
    )
}

export default Chatting;
