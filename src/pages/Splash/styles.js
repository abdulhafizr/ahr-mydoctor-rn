import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../utils';

const styles = StyleSheet.create({
	container: {
		  flex: 1,
		  alignItems: 'center',
		  justifyContent: 'center',
	},
	title: {
		fontSize: 20,
		marginTop: 20,
		color: colors.text.primary,
		fontFamily: fonts.primary[200], 
	}
});

export default styles;