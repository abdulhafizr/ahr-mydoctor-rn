import React, { useEffect } from 'react';
import { View, Text } from 'react-native';
import { ILLogo } from '../../assets';
import { firebase } from '../../config';
import styles from './styles.js';

const Splash = ({navigation}) => {
	useEffect(() => {
		const unsubscribe = firebase.auth().onAuthStateChanged((user) => { 
			setTimeout(() => {
				if(user) {
					navigation.replace('MainApp');
				}else{
					navigation.replace('GetStarted');
				}
			}, 2300);
		});

		return () => unsubscribe();
	}, [navigation])
	return (
	  <View style={styles.container}>
	  	<ILLogo />
	  	<Text style={styles.title}>AHR MyDoctor</Text>
	  </View>
	);
}

export default Splash;