import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../utils';

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.primary,
        flex: 1,
    },
    content: {
        backgroundColor: colors.white,
        flex: 1,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        overflow: 'hidden',
    },
    welcome: {
        fontSize: 20,
        fontFamily: fonts.primary[400],
        color: colors.primary,
        maxWidth: 209,
        marginTop: 30,
        marginBottom: 16,
        lineHeight: 24,
    },
    categoryWrapper: {
        height: 130,
    },
    category: {
        flexDirection: 'row',
    },
    topRated: {
        marginTop: 30,
        marginBottom: 16,
        fontSize: 16,
        fontFamily: fonts.primary[600],
    },
    goodNews: {
        marginTop: 14,
        marginBottom: 16,
        fontSize: 16,
        fontFamily: fonts.primary[600],
        paddingLeft: 16,
    },
    sectionWrapper: {
        paddingHorizontal: 16,
    },
});

export default styles;
