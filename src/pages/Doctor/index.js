import React, {useState, useEffect} from 'react';
import { Text, View, ScrollView } from 'react-native';
import { DoctorCategory, Gap, HeaderProfile, NewsItem, RatedDoctor } from '../../components';
import { firebase } from '../../config';
import { getData, messageError } from '../../utils';
import styles from './styles.js';

const Doctor = ({navigation}) => {
    const [user, setUser] = useState({});
    const [news, setNews] = useState([]);
    const [doctorCategories, setDoctorCategories] = useState([]);
    const [top_doctors, setTop_doctors] = useState([]);

    useEffect(() => {
        navigation.addListener('focus', () => {
            getUser();
        })
        getNews();
        getDoctorCategories();
        getTopDoctors();

    }, [navigation]);
    const getUser = () => {
        getData('user').then((response) => {
            setUser(response)
        })
    }
    const getNews = () => {
        firebase.database()
        .ref('news')
        .once('value')
        .then((response) => {
            if(response.val()) {
                const data = response.val();
                const filterData = data.filter((item) => item !== null);
                setNews(filterData);
            }
        })
        .catch((error) => {
            messageError(error.message);
        })
    }
    const getDoctorCategories = () => {
        firebase.database()
        .ref('doctor_categories')
        .once('value')
        .then((response) => {
            if(response.val()) {
                const data = response.val();
                const filterData = data.filter((item) => item !== null);
                setDoctorCategories(filterData);
            }
        })
    }
    const getTopDoctors = () => {
        firebase.database()
        .ref('doctors')
        .orderByChild('rate')
        .limitToLast(3)
        .once('value')
        .then((response) => {
            if(response.val()) {
                const doctors = response.val();
                const data = [];
                Object.keys(doctors).map((key) => {
                    data.push({
                        id: key,
                        data: doctors[key]
                    })
                })
                setTop_doctors(data);
            }
        })
    }

    return (
        <View style={styles.container}>
            <View style={styles.content}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={styles.sectionWrapper}>
                        <Gap height={30} />
                        <HeaderProfile fullName={user.fullName} job={user.job} navigation={navigation} onPress={() => navigation.navigate('UserProfile')} />
                        <Text style={styles.welcome}>Mau konsultasi dengan siapa hari ini?</Text>
                    </View>
                    <ScrollView horizontal showsHorizontalScrollIndicator={false} style={styles.categoryWrapper}>
                        <Gap width={16} />
                        <View style={styles.category}>
                            {
                                doctorCategories.map((category) => (
                                    <DoctorCategory 
                                        key={category.id} 
                                        category={category.category} 
                                        onPress={() => navigation.navigate('ChooseDoctor', category.category)}  
                                    />        
                                ))
                            }
                        </View>
                        <Gap width={6} />
                    </ScrollView>
                    <View style={styles.sectionWrapper}>
                        <Text style={styles.topRated}>Top Rated Doctors</Text>
                        {
                            top_doctors.map((doctor) => (
                                <RatedDoctor 
                                    key={doctor.id} 
                                    name={doctor.data.fullName} 
                                    category={doctor.data.category}
                                    image={doctor.data.photo} 
                                    onPress={() => navigation.navigate('DoctorProfile', 
                                        {
                                            ...doctor.data   
                                        })
                                    }
                                />        
                            ))
                        }
                    </View>
                    <View>
                        <Text style={styles.goodNews}>GoodNews</Text>
                        <View>
                            {
                                news.map((item) => (
                                    <NewsItem 
                                        key={item.title}
                                        title={item.title}
                                        image={item.image}
                                        timestamp={item.timestamp} 
                                    />            
                                ))
                            }
                        </View>
                        <Gap height={30} />
                    </View>
                </ScrollView>
            </View>
        </View>
    )
}

export default Doctor

