import React, { useState } from 'react';
import { Image, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { useSelector } from 'react-redux';
import { ICAddBtnPhoto, ICRemoveBtnPhoto, ILUserDefaultPhoto } from '../../assets';
import { Button, Gap, Header, Link } from '../../components';
import { launchImageLibrary } from 'react-native-image-picker';
import { storeData, messageSuccess, messageError } from '../../utils';
import { firebase } from '../../config';
import styles from './styles';
 
const UploadPhoto = ({navigation, route}) => {
    const globalState = useSelector((state) => state);
    const { fullName, job, uid } = route.params;
    const [hashPhoto, setHashPhoto] = useState(false);
    const [photoForDB, setPhotoForDB] = useState('');
    const [photo, setPhoto] = useState(ILUserDefaultPhoto);
    const getImage = () => {
        launchImageLibrary({includeBase64: true, quality: 0.8, maxHeight: 210, maxWidth: 210}, (response) => {
            if(response.didCancel) {
                messageError('Photo gagal dipilih!');
            }else{
                const source = {uri: response.uri};
                setPhoto(source);
                setHashPhoto(true);
                setPhotoForDB(`data:${response.type};base64, ${response.base64}`);
            }
        })
    }
    const uploadAndContinue = () => {
        firebase.database()
            .ref(`users/${uid}/`)
            .update({photo: photoForDB})
            .then(() => {
                messageSuccess('Photo Profile berhasil ditambahkan!');
            })
            .catch(() => {
                messageError('Photo Profile gagal ditambahkan!');
            })
        
        const data = {...route.params, photo: photoForDB};
        storeData('user', data);

        navigation.replace('MainApp');
    }
    const skipUploadPhoto = () => {
        firebase.database()
            .ref(`users/${uid}/`)
            .update({photo: globalState.defaultUser})
            .then(() => {
                messageSuccess('Photo Profile berhasil ditambahkan!');
            })
            .catch(() => {
                messageError('Photo Profile gagal ditambahkan!');
            })
        const data = {...route.params, photo: globalState.defaultUser};
        storeData('user', data);
        navigation.replace('MainApp');
    }
    return (
        <View style={styles.container}>
            <Header title="Upload Photo" onPress={() => navigation.replace('GetStarted')} />
            <ScrollView>
            <View style={styles.content}>
                <View style={styles.uploadImage}>
                    <View style={styles.avatar}>
                        <Image source={photo} style={styles.avatarImage} />
                        <TouchableOpacity style={styles.btnAdd} onPress={getImage}>
                            {
                                hashPhoto ? <ICRemoveBtnPhoto /> : <ICAddBtnPhoto />
                            }
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.captionName}>{fullName}</Text>
                    <Text style={styles.captionJob}>{job}</Text>
                </View>
                <View style={styles.buttonWrapper}>
                    <Button disable={!hashPhoto} onPress={uploadAndContinue} title="Upload and Continue" />
                    <Gap height={30} />
                    <Link label="Skip for this" align="center" size={16} onPress={skipUploadPhoto} />
                </View>
            </View>
            </ScrollView>
        </View>
    )
}

export default UploadPhoto;
