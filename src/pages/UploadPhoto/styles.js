import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../utils';

const styles = StyleSheet.create({
    avatar: {
        width: 130,
        height: 130,
        borderRadius: 130 / 2,
        borderColor: colors.gray2,
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
    },
    avatarImage: {
        width: 110,
        height: 110,
        borderRadius: 110 /2,
    },
    btnAdd: {
        position: 'absolute',
        bottom: 8,
        right: 6
    },
    captionName: {
        fontFamily: fonts.primary[600],
        fontSize: 24,
        marginTop: 26,
        textAlign: 'center',
        color: colors.text.primary,
    },
    captionJob: {
        fontFamily: fonts.primary[400],
        fontSize: 18,
        textAlign: 'center',
        marginTop: 4,
    },
    content: {
        padding: 40,
        flex: 1,
        justifyContent: 'space-between',
        paddingBottom: 64,
        paddingTop: 0,
    },
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    uploadImage: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        marginTop: 86,
    },
    buttonWrapper: {
        marginTop: 88,
    },
});

export default styles;