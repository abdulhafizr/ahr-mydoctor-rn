import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../utils';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: colors.white,
	},
	content: {
		paddingHorizontal: 40,
		paddingBottom: 30,
		paddingTop: 10,
	},
});

export default styles;
