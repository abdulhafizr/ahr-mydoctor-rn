import React, { useState } from 'react';
import { ScrollView, View } from 'react-native';
import { Header, Input, Button, Gap } from '../../components';
import { firebase } from '../../config';
import { storeData, messageError, messageSuccess } from '../../utils';
import styles from './styles.js';

const Signup = ({navigation}) => {
	const [loading, setLoading] = useState(false);
	const [form, setForm] = useState({
		fullName: '',
		job: '',
		email: '',
		password: '',
		disableButton: true,
	})
	const onInputChange = (key, value) => {
		setForm({
			...form,
			[key] : value,
			disableButton: (form.fullName.length >= 0 && form.job.length >= 0 && form.email.includes('@') && form.password.length >= 5) ? false : true,
		})
	}
	const SignupUser = () => {
		const {email, password, fullName, job} = form;

		setLoading(!loading);
		firebase.auth().createUserWithEmailAndPassword(email, password)
			.then((user) => {
				const data =  {uid: user.user.uid, email, fullName, job};
				setLoading(false);
				firebase.database()
					.ref(`/users/${data.uid}/`)
					.set(data);
				setForm({
					fullName: '',
					job: '',
					email: '',
					password: '',
				});
				storeData('user', data);
				messageSuccess('Signup Successfully');
				navigation.replace('UploadPhoto', data);
			})
			.catch((error) => {
				setLoading(false);
				messageError(error.message);
			});
	}
    return (
		<View style={styles.container}>
			<ScrollView showsVerticalScrollIndicator={false}>
				<Header title="Signup" onPress={() => navigation.goBack()} />
				<View style={styles.content}>
					<Input 
						label="Full Name" 
						value={form.fullName} 
						onChangeText={(value) => onInputChange('fullName', value)} 
					/>
					<Gap height={24} />
					<Input 
						label="Job" 
						value={form.job}
						onChangeText={(value) => onInputChange('job', value)} 
					/>
					<Gap height={24} />
					<Input 
						label="Email Address" 
						value={form.email}
						onChangeText={(value) => onInputChange('email', value)} 
					/>
					<Gap height={24} />
					<Input label="Password" 
						secureTextEntry 
						value={form.password}
						onChangeText={(value) => onInputChange('password', value)} 
					/>
					<Gap height={40} />
					{
						form.disableButton ? <Button disable title="Continue" /> : <Button isLoading={loading} title="Continue" onPress={SignupUser} />
					}
				</View>
			</ScrollView>
		</View>
    );
}

export default Signup;
