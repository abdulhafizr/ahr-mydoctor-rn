import React, {useState, useEffect} from 'react';
import { ImageBackground, Text, View, ScrollView } from 'react-native';
import { ILHospitalBg } from '../../assets';
import { firebase } from '../../config';
import { messageError } from '../../utils';
import { HospitalList } from '../../components';
import styles from './styles.js';

const Hospitals = () => {
    const [hospitals, setHospitals] = useState([]);
    useEffect(() => {
        firebase.database()
            .ref(`hospitals`)
            .once('value')
            .then((response) => {
                if(response.val()) {
                    setHospitals(response.val());
                }
            })
            .catch((error) => {
                messageError(error.message);
            });
    }, [])
    return (
        <View style={styles.container}>
            <ImageBackground source={ILHospitalBg} style={styles.hospitalBg}>
                <Text style={styles.title}>Nearby Hospitals</Text>
                <Text style={styles.subtitle}>{hospitals.length-1} tersedia</Text>
            </ImageBackground>
            <View style={styles.content}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    {
                        hospitals.map((hospital) => (
                            <HospitalList 
                                key={hospital.id} 
                                name={hospital.name} 
                                address={hospital.address} 
                                image={hospital.image}
                            />
                        ))
                    }
                </ScrollView>
            </View>

        </View>
    )
}

export default Hospitals

