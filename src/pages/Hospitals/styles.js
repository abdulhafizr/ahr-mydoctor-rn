import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../utils';

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.primary,
        flex: 1,
    },
    content: {
        backgroundColor: colors.white,
        flex: 1,
        borderRadius: 20,
        marginTop: -30,
        overflow: 'hidden',
    },
    hospitalBg: {
        height: 240,
        width: '100%',
        paddingTop: 30,
    },
    title: {
        fontSize: 20,
        fontFamily: fonts.primary[600],
        color: colors.white,
        textAlign: 'center',
    },
    subtitle: {
        fontSize: 14,
        color: colors.white,
        textAlign: 'center',
        fontFamily: fonts.primary[300],
        marginTop: 6,
    }
});

export default styles;
