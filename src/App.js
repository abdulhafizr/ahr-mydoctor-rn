import React from 'react';
import { LogBox } from 'react-native';
import { Router, store } from './config';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { Provider } from 'react-redux';
import FlashMessage from "react-native-flash-message";

LogBox.ignoreAllLogs(['Setting a timer']);

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Router />
      </NavigationContainer>
      <FlashMessage position="top" />
    </Provider>
  );
}

export default App;