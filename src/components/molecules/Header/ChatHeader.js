import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { Button } from '../../atoms';
import { colors, fonts } from '../../../utils';

const ChatHeader = ({title, profession, photo, onPress}) => {
    return (
        <View style={styles.container}>
            <Button type="icon-only" isWhiteIcon={true} onPress={onPress} />
            <View style={styles.content}>
                <Text style={styles.name}>{title}</Text>
                <Text style={styles.type}>{profession}</Text>
            </View>
            <Image source={{uri: photo}} style={styles.avatar} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingVertical: 30,
        paddingHorizontal: 16,
        backgroundColor: colors.primary,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
    },
    content: {
        flex: 1,
    },
    avatar: {
        width: 46,
        height: 46,
        borderRadius: 46 / 2,
    },
    name: {
        fontSize: 20,
        fontFamily: fonts.primary[600],
        color: colors.white,
        textAlign: 'center',
        textTransform: 'capitalize',
    },
    type: {
        fontSize: 14,
        fontFamily: fonts.primary[400],
        color: colors.gray1,
        textAlign: 'center',
        marginTop: 6,
        textTransform: 'capitalize',
    },
});

export default ChatHeader

