import React from 'react';
import { View, Text } from 'react-native';
import { Gap, Button } from '../../../components';
import ChatHeader from './ChatHeader';
import styles from './styles';

const Header = ({title, profession, photo, onPress, type}) => {
  if(type === 'chat header') {
    return <ChatHeader title={title} profession={profession} photo={photo} onPress={onPress} />
  }
    return (
      <View style={styles.container(type)}>
      	<Button type="icon-only" onPress={onPress} isWhiteIcon={(type === 'choose doctor' ? true : false)} />
      	<Text style={styles.title(type)}>{title}</Text>
      	<Gap width={24} />
      </View>
    );
}

export default Header;
