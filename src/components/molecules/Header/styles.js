import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../utils';

const styles = StyleSheet.create({
	container: (type) => (
		{
			paddingHorizontal: 16,
			paddingVertical: 30,  
			flexDirection: 'row',
			justifyContent: 'space-between',
			alignItems: 'center',
			backgroundColor: (type === 'choose doctor' ? colors.primary : colors.white),
			borderBottomLeftRadius: (type === 'choose doctor' ? 20 : 0),
			borderBottomRightRadius: (type === 'choose doctor' ? 20 : 0),
		}
	),
	title: (type) => (
		{
			color: (type === 'choose doctor' ? colors.white : colors.text.primary),
			flex: 1,
			textAlign: 'center',
			fontSize: 20,
			fontFamily: fonts.primary[600], 
			textTransform: 'capitalize',
		}
	),
});

export default styles;
