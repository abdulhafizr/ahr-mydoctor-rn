import React from 'react';
import { View } from 'react-native';
import Doctor from './Doctor';
import User from './User';

const BubbleChat = ({photo, message, timestamp, isDoctor}) => {
    return (
        <View style={{paddingHorizontal: 16}}>
            {
                (isDoctor) ? 
                (
                    <Doctor message={message} timestamp={timestamp} photo={photo} />
                ) : (
                    <User message={message} timestamp={timestamp} />
                )
            }
        </View>
    )
}

export default BubbleChat;
