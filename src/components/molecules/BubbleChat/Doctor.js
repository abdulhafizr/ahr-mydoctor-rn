import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { colors, fonts } from '../../../utils'

const Doctor = ({photo, message, timestamp}) => {
    return (
        <View style={styles.container}>
            <Image source={{uri: photo}} style={styles.avatar} />
            <View>
                <View style={styles.content}>
                    <Text style={styles.message}>{message}</Text>
                </View>
                <Text style={styles.timestamp}>{timestamp}</Text>
            </View>
        </View>
    )
}

export default Doctor

const styles = StyleSheet.create({
    container: {
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'flex-end',
    },
    content: {
        maxWidth: 189,
        padding: 12,
        backgroundColor: colors.secondary,
        borderRadius: 10,
        borderBottomLeftRadius: 0,
    },
    message: {
        fontSize:14,
        fontFamily: fonts.primary[400],
        color: colors.white,
        lineHeight: 16.8,
    },
    timestamp: {
        fontSize: 11,
        fontFamily: fonts.primary[400],
        color: colors.gray1,
        marginTop: 8,
    },
    avatar: {
        width: 30,
        height: 30,
        borderRadius: 30 / 2,
        marginRight: 12,
    }
})
