import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { colors, fonts } from '../../../utils';

const User = ({message, timestamp}) => {
    return (
        <View style={styles.container}>
            <View style={styles.content}>
                <Text style={styles.message}>{message}</Text>
            </View>
            <Text style={styles.timestamp}>{timestamp}</Text>
        </View>
    )
}

export default User;

const styles = StyleSheet.create({
    container: {
        maxWidth: 210,
        alignSelf: 'flex-end',
        marginBottom: 20,
    },
    content: {
        backgroundColor: colors.lightteal,
        padding: 12,
        borderRadius: 10,
        borderBottomRightRadius: 0,
    },
    message: {
        fontSize: 14,
        fontFamily: fonts.primary[400],
        color: colors.primary,
        lineHeight: 16.8,
    },
    timestamp: {
        fontSize: 11,
        fontFamily: fonts.primary[400],
        color: colors.gray1,
        marginTop: 8,
        textAlign: 'right'
    },
})
