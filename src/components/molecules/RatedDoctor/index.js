import React from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import { ICStarRate } from '../../../assets/index.js';
import styles from './styles.js';

const RatedDoctor = ({name, category, image, onPress}) => {
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <Image source={{uri: image}} style={styles.profile} />
            <View style={styles.caption}>
                <Text style={styles.doctorName}>{name}</Text>
                <Text style={styles.doctorType}>{category}</Text>
            </View>
            <View style={styles.starsWrapper}>
                <ICStarRate />
                <ICStarRate />
                <ICStarRate />
                <ICStarRate />
                <ICStarRate />
            </View>
        </TouchableOpacity>
    )
}

export default RatedDoctor
