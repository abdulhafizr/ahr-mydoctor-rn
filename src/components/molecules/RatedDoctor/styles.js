import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../utils';

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: 16,
    },
    profile: {
        width: 50,
        height: 50,
        marginRight: 12,
    },
    caption: {
        flex: 1,
    },
    doctorName: {
        fontSize: 16,
        fontFamily: fonts.primary[600],
        color: colors.primary,
    },
    doctorType: {
        marginTop: 2,
        fontSize: 12,
        fontFamily: fonts.primary[400],
        color: colors.gray1,
    },
    starsWrapper: {
        flexDirection: 'row'
    },
});

export default styles;
