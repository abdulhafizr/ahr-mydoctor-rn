import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../utils';

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        paddingBottom: 12,
        borderBottomColor: colors.gray3,
        borderBottomWidth: 1,
        marginBottom: 16,
        paddingHorizontal: 16,
    },
    newsImage: {
        width: 80,
        height: 60,
        borderRadius: 11,
    },
    caption: {
        flex: 1,
    },
    title: {
        width: '90%',
        fontSize: 16,
        fontFamily: fonts.primary[600],
        color: colors.primary,
    },
    timestamp: {
        fontSize: 12,
        fontFamily: fonts.primary[400],
        color: colors.gray1,
    },
})

export default styles;
