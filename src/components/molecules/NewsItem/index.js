import React from 'react';
import { Text, View, Image } from 'react-native';
import styles from './styles.js';

const NewsItem = ({title, timestamp, image}) => {
    return (
        <View style={styles.container}>
            <View style={styles.caption}>
                <Text style={styles.title}>{title}</Text>
                <Text style={styles.timestamp}>{timestamp}</Text>
            </View>
            <Image source={{uri: image}} style={styles.newsImage} />
        </View>
    )
}

export default NewsItem
