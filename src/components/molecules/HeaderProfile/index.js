import React, {useEffect, useState} from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { ILUserDefaultPhoto } from '../../../assets';
import { getData } from '../../../utils';
import styles from './styles.js';

const HeaderProfile = ({fullName, job, onPress}) => {
    const [user, setUser] = useState({
        fullName: 'Abdul',
        job: 'React',
        photo: '',
    })
    useEffect(() => {
        getData('user').then((response) => {
            const data = {...response, photo: {uri: response.photo}};
            setUser(data);
        })
    }, []);
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={onPress}>
                <Image source={(user.photo.uri == undefined ? ILUserDefaultPhoto : user.photo)} style={styles.profile} />
            </TouchableOpacity>
            <View style={styles.caption}>
                <Text style={styles.captionName}>{fullName}</Text>
                <Text style={styles.captionJob}>{job}</Text>
            </View>
        </View>
    )
}

export default HeaderProfile
