import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../utils';

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    profile: {
        width: 46,
        height: 46,
        borderRadius: 46 / 2,
    },
    caption: {
        marginLeft: 12,
    },
    captionName: {
        fontSize: 16,
        fontFamily: fonts.primary[600],
        color: colors.primary,
        textTransform: 'capitalize',
    },
    captionJob: {
        fontSize: 12,
        fontFamily: fonts.primary[400],
        color: colors.gray1,
        textTransform: 'capitalize',
    },
})

export default styles;
