import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { ICRemoveBtnPhoto, ILUserDefaultPhoto } from '../../../assets';
import styles from './styles.js';

const Profile = ({fullName, job, photo, remove, onPress}) => {
    return (
        <View style={styles.container}>
            <View style={styles.profileWrapper}>
                <Image source={{uri: photo}} style={styles.profile} />
                {
                    remove && (
                        <TouchableOpacity style={styles.btn} onPress={onPress}>
                            <ICRemoveBtnPhoto />
                        </TouchableOpacity>
                    )
                }
            </View>
            {
                fullName && (
                    <View style={styles.caption}>
                        <Text style={styles.name}>{fullName}</Text>
                        <Text style={styles.job}>{job}</Text>
                    </View>
                )
            }
        </View>
    )
}

export default Profile;
