import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../utils';

const styles = StyleSheet.create({
    container: {
        marginVertical: 10,
        alignItems: 'center',
    },
    profileWrapper: {
        width: 130,
        height: 130,
        borderWidth: 1,
        borderColor: colors.gray2,
        borderRadius: 130 /2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    profile: {
        width: 110,
        height: 110,
        borderRadius: 110 /2,
    },
    caption: {
        marginTop: 16,
    },
    name: {
        fontSize: 20,
        fontFamily: fonts.primary[600],
        textAlign: 'center',
        color: colors.primary,
    },
    job: {
        fontSize: 16,
        fontFamily: fonts.primary[400],
        textAlign: 'center',
        color: colors.gray1,
        marginTop: 2,
    },
    btn: {
        position: 'absolute',
        right: 6,
        bottom: 8,
    }
})

export default styles;
