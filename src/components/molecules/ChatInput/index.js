import React, {useState, useEffect} from 'react';
import { View, TextInput } from 'react-native';
import { getData, getChatTime, getChatYear, messageError } from '../../../utils';
import { firebase } from '../../../config';
import { Button } from '../../atoms/index.js';
import styles from './styles.js';

const ChatInput = ({doctorUid, doctorName}) => {
    const [user, setUser] = useState({});
    const [message, setMessage] = useState('');
    useEffect(() => {
        getData('user').then((response) => {
            setUser(response);
        })
    }, []);

    const sendMessage = () => {
        const today = new Date();
        const data = {
            sentBy: user.uid,
            chatDate: today.getTime(),
            chatTime: getChatTime(today),
            message,
        }

        const chatIdUser = `${user.uid}_${doctorUid}`;
        const chatIdDoctor = `${doctorUid}_${user.uid}`;
        const uri = `chatting/${chatIdUser}/allChat/${getChatYear(today)}`;
        firebase.database()
            .ref(uri)
            .push(data)
            .then(() => {
                setMessage('');

                // Make history chat for user
                firebase.database()
                    .ref(`messages/${user.uid}/${chatIdUser}`)
                    .set({
                        lastChat: message,
                        lastDate: today.getTime(),
                        uidPartner: doctorUid,
                    })
                
                // Make history chat for doctor
                firebase.database()
                    .ref(`messages/${doctorUid}/${chatIdDoctor}`)
                    .set({
                        lastChat: message,
                        lastDate: today.getTime(),
                        uidPartner: user.uid,
                    })
            })

            .catch((error) => messageError(error.message));
    }
    return (
        <View style={styles.container}>
            <TextInput 
                style={styles.chatInput} 
                placeholder={`Tulis pesan untuk ${doctorName.split(' ')[0]}`} 
                value={message}
                onChangeText={(value) => setMessage(value)}
            />
            <Button 
                disable={message.length < 1} 
                onPress={sendMessage}
                type="btn-chat" 
            />
        </View>
    )
}

export default ChatInput;
