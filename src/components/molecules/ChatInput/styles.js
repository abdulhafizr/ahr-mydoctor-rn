import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../utils';

const styles = StyleSheet.create({
  container: {
    padding: 16,
    flexDirection: 'row',
    backgroundColor: colors.white,
  },
    chatInput: {
      backgroundColor: colors.gray4,
      borderRadius: 10,
      flex: 1,
      marginRight: 10,
      maxHeight: 45,
      padding: 14,
      fontSize: 14,
      fontFamily: fonts.primary[400],
      color: colors.primary,
      
  }  
})

export default styles;
