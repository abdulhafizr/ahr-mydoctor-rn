import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { ICEditProfile, ICHelpCenter, ICLanguage, ICRate, ICRightBlack } from '../../../assets/index.js';
import styles from './styles.js';

const List = ({onPress, image, name, submenu, rightArrow, icon}) => {
    const Icon = () => {
        switch(icon) {
            case 'language':
                return <ICLanguage />
            case 'give-us-rate':
                return <ICRate />
            case 'help-center':
                return <ICHelpCenter />
            default:
                return <ICEditProfile />
        }
    }
    return (
        <TouchableOpacity onPress={onPress} style={styles.container}>
            {
                icon ? (
                    <Icon style={styles.icon} />
                ) : (
                    <Image source={{uri: image}} style={styles.avatar} />
                )
            }
            <View style={styles.caption}>
                <Text style={styles.name}>{name}</Text>
                <Text style={styles.message}>{submenu}</Text>
            </View>
            {
                rightArrow && (
                    <ICRightBlack />
                )
            }
        </TouchableOpacity>
    )
}

export default List;
