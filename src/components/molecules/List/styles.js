import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../utils';

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        paddingHorizontal: 16,
        paddingVertical: 20,
        borderBottomColor: colors.gray3,
        borderBottomWidth: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    caption: {
        flex: 1,
        marginLeft: 12,
    },
    avatar: {
        width: 46,
        height: 46,
    },
    name: {
        fontSize: 16,
        fontFamily: fonts.primary[400],
        color: colors.primary,
    },
    message: {
        color: colors.gray1,
        fontSize: 12,
        fontFamily: fonts.primary[300],
        textTransform: 'capitalize',
    },
    icon: {
        width: 24,
        height: 24,
    },
});

export default styles;
