import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../utils';

const styles = StyleSheet.create({
    container: {
        padding: 16,
        borderBottomColor: colors.gray3,
        borderBottomWidth: 1,
    },
    label: {
        fontSize: 14,
        fontFamily: fonts.primary[400],
        color: colors.gray1,
    },
    value: {
        fontSize: 14,
        fontFamily: fonts.primary[400],
        color: colors.primary,
        marginTop: 6,
    }
})

export default styles;
