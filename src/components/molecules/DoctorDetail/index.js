import React from 'react';
import { Text, View } from 'react-native';
import styles from './styles.js';

const DoctorDetail = ({label, value}) => {
    return (
        <View style={styles.container}>
            <Text style={styles.label}>{label}</Text>
            <Text style={styles.value}>{value}</Text>
        </View>
    )
}

export default DoctorDetail;
