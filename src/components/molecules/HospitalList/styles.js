import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../utils';

const styles = StyleSheet.create({
    container: {
        padding: 16,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: colors.gray3,
        borderBottomWidth: 1,
    },
    hospitalImage: {
        height: 60,
        width: 80,
        borderRadius: 11,
        marginRight: 16,
    },
    hospitalName: {
        maxWidth: 151,
        fontSize: 16,
        fontFamily: fonts.primary[400],
        color: colors.primary,
        lineHeight: 19.2,
    },
    hospitalAddress: {
        fontSize: 12,
        fontFamily: fonts.primary[300],
        color: colors.gray1,
        lineHeight: 16.37,
    },
});

export default styles;