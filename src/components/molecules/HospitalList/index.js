import React from 'react'
import { Image, Text, View } from 'react-native';
import styles from './styles.js';

const HospitalList = ({name, address, image}) => {
    return (
        <View style={styles.container}>
            <Image source={{uri: image}} style={styles.hospitalImage} />
            <View>
                <Text style={styles.hospitalName}>{name}</Text>
                <Text style={styles.hospitalAddress}>{address}</Text>
            </View>
        </View>
    )
}

export default HospitalList
