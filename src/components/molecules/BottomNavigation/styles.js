import { StyleSheet} from 'react-native';
import { colors } from '../../../utils/colors';

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        paddingHorizontal: 53,
        paddingVertical: 12,
        justifyContent: 'space-between',
        backgroundColor: colors.primary,
    }
})

export default styles;
