import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { ICDoctorAnak, ICDoctorObat, ICDoctorPsikiater, ICDoctorUmum } from '../../../assets/index.js';
import styles from './styles.js';

const DoctorCategory = ({category, onPress}) => {
    const Icon =() => {
        switch(category) {
            case 'psikiater':
                return <ICDoctorPsikiater />
            case 'dokter obat':
                return <ICDoctorObat />
            case 'dokter anak':
                return <ICDoctorAnak />
            default:
                return <ICDoctorUmum />
        }
    }
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <Icon />
            <View style={styles.captionWrapper}>
                <Text style={styles.caption}>Saya butuh</Text>
                <Text style={styles.category}>{category}</Text>
            </View>
        </TouchableOpacity>
    )
}

export default DoctorCategory
