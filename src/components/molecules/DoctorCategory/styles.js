import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../utils';

const styles = StyleSheet.create({
    container: {
        padding: 12,
        width: 100,
        height: 130,
        backgroundColor: colors.softTeal,
        borderRadius: 10,
        marginRight: 10,
    },
    captionWrapper: {
        marginTop: 28,
    },
    caption: {
        fontSize: 12,
        fontFamily: fonts.primary[300],
    },
    category: {
        fontSize: 12,
        fontFamily: fonts.primary[600],
        textTransform: 'capitalize',
    }
})

export default styles;
