import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../utils';

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    caption: (isFocused) => (
        {
            color: isFocused ? colors.secondary : colors.deepGray,
            marginTop: 4,
            fontFamily: fonts.primary[600],
            fontSize: 10,
        }
    )
})

export default styles;
