import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { ICDoctor, ICDoctorActive, ICMessages, ICMessagesActive, ICHospitals, ICHospitalsActive } from '../../../assets';
import styles from './styles.js';

const TabItem = ({label, isFocused, onPress, onLongPress}) => {
    const Icon = () => {
        if(label === 'Doctor') {
            return isFocused ? <ICDoctorActive /> : <ICDoctor />;
        }
        if(label === 'Messages') {
            return isFocused ? <ICMessagesActive /> : <ICMessages />;
        }if(label === 'Hospitals') {
            return isFocused ? <ICHospitalsActive /> : <ICHospitals />;
        }
    }
    return (
        <TouchableOpacity style={styles.container} onPress={onPress} onLongPress={onLongPress}>
            <Icon />
            <Text style={styles.caption(isFocused)}>{label}</Text>
        </TouchableOpacity>
    )
}

export default TabItem
