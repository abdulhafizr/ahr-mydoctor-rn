import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { ICBtnChatDark, ICBtnChatLight } from '../../../assets';
import { colors } from '../../../utils';

const BtnChat = ({onPress, disable}) => {
    if(disable) {
        return (
            <View style={styles.disable} onPress={onPress}>
                <ICBtnChatDark />
            </View>
        )
    }
    return (
        <TouchableOpacity style={styles.enable} onPress={onPress}>
            <ICBtnChatLight />
        </TouchableOpacity>
    )
}

export default BtnChat

const styles = StyleSheet.create({
    container: (isWhiteIcon) => (
        {
            width: 45,
            height: 45,
            backgroundColor: (isWhiteIcon ? colors.blue1 : colors.gray4),
            borderRadius: 10,
            paddingTop: 3.09,
            paddingLeft: 8.09,
            paddingBottom: 8.09,
        }
    ),
    disable: {
        width: 45,
        height: 45,
        backgroundColor: colors.gray4,
        borderRadius: 10,
        paddingTop: 3.09,
        paddingLeft: 8.09,
        paddingBottom: 8.09,
    },
    enable: {
        width: 45,
        height: 45,
        backgroundColor: colors.blue1,
        borderRadius: 10,
        paddingTop: 3.09,
        paddingLeft: 8.09,
        paddingBottom: 8.09,
    },
})
