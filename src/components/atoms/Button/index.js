import React from 'react';
import { TouchableOpacity, Text, View } from 'react-native';
import BtnChat from './BtnChat';
import BtnLoading from './BtnLoading';
import IconOnly from './IconOnly';
import styles from './styles';

const Button = ({ type, title, onPress, disable, isWhiteIcon, isLoading }) => {
  if(type === 'icon-only') {
		return <IconOnly onPress={onPress} isWhiteIcon={isWhiteIcon} />
  }
  if(type === 'btn-chat') {
		return <BtnChat disable={disable} onPress={onPress} />
  }
  if(isLoading) {
		return <BtnLoading />
  }
  if(disable) {
		return (
      <View style={styles.diableBtn} onPress={onPress}>
        <Text style={styles.disableText}>{title}</Text>
      </View>
    )
	}
  return (
    <TouchableOpacity style={styles.container(type)} onPress={onPress}>
      <Text style={styles.title(type)}>{title}</Text>
    </TouchableOpacity>
  )
}

export default Button;
