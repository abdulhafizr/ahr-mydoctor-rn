import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../utils';

const styles = StyleSheet.create({
	container: (type) => (
		{
			backgroundColor: type === 'secondary'? colors.button.secondary : colors.button.primary,
			paddingVertical: 10,
			borderRadius: 10,
		}
	),
	diableBtn: {
		paddingVertical: 10,
		borderRadius: 10,
		backgroundColor: colors.gray2,
	},
	disableText: {
		textAlign: 'center',
		fontSize: 18,
		fontFamily: fonts.primary[600],
		color: colors.gray1,
	},
	title: (type) => (
		{
			color: type === 'secondary' ? colors.text.primary : colors.text.white,
			textAlign: 'center',
			fontSize: 18,
			fontFamily: fonts.primary[600],
		}
	),
	btnLoadingContainer: {
		paddingVertical: 10,
		backgroundColor: colors.button.loading,
		borderRadius: 10,
		flexDirection: 'row',
		justifyContent: 'center',
	},
	btnLoadingTitle: {
		textAlign: 'center',
		color: colors.secondary,
		fontSize: 18,
		fontFamily: fonts.primary[600],
		marginLeft: 10,
	},
});

export default styles;