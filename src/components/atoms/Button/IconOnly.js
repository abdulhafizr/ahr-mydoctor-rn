import React from 'react';
import { TouchableOpacity } from 'react-native';
import { ICArrowBackBlack, ICArrowBackWhite } from '../../../assets';

const IconOnly = ({onPress, isWhiteIcon}) => {
    const Icon = () => {
    	if(isWhiteIcon) {
    		return <ICArrowBackWhite />;
    	}
    	return <ICArrowBackBlack />;
    }
    return (
      <TouchableOpacity onPress={onPress}>
      	<Icon />
      </TouchableOpacity>
    );
}

export default IconOnly;
