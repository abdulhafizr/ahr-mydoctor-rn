import React from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import { colors } from '../../../utils';
import styles from './styles';

const BtnLoading = () => {
  return (
    <View style={styles.btnLoadingContainer}>
        <ActivityIndicator color={colors.secondary} />
        <Text style={styles.btnLoadingTitle}>Loading...</Text>
    </View>
  )
}

export default BtnLoading;
