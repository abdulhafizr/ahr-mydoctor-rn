import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../utils';

const styles = StyleSheet.create({
	label: {
		fontSize: 16,
		color: colors.text.secondary,
		fontFamily: fonts.primary[400],
	},
	input: (color) => (
		{
			borderWidth: 1,
			borderRadius: 10,
			borderColor: color,
			marginTop: 6,
			padding: 12,
			maxHeight: 45,
		}
	)
});

export default styles;
