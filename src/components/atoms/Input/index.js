import React, { useState } from 'react';
import { TextInput, Text, View } from 'react-native';
import { colors } from '../../../utils/index.js';
import styles from './styles.js';

const Input = ({label, value, onChangeText, secureTextEntry, disable}) => {
  const [border, setBorder] = useState(colors.gray2);
    return (
      <View>
      	<Text style={styles.label}>{label}</Text>
        <TextInput 
          onFocus={() => setBorder(colors.secondary)}
          onBlur={() => setBorder(colors.gray2)} 
          style={styles.input(border)}
          value={value}
          onChangeText={onChangeText}
          secureTextEntry={secureTextEntry}
          editable={!disable}
          selectTextOnFocus={!disable}
        />
      </View>
    );
}

export default Input;
