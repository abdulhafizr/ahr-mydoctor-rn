import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../utils';

const styles = StyleSheet.create({
	link: (size, align) => (
		{
			fontSize: size || 16,
			color: colors.text.secondary,
			fontFamily: fonts.primary[400],
			textDecorationLine: 'underline',
			textAlign: align ||'left',
		}
	)
});

export default styles;
