import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import styles from './styles.js';

const Link = ({label, size, align, onPress}) => {
    return (
      <TouchableOpacity onPress={onPress}>
      	<Text style={styles.link(size, align)}>{label}</Text>
      </TouchableOpacity>
    );
}

export default Link;
