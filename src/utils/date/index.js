export const getChatTime = (date) => {
    const hours = date.getHours();
    const minutes = date.getMinutes();
    
    return `${hours}.${minutes} ${hours >= 12 ? 'PM' : 'AM'}`;
}

export const getChatYear = (date) => {
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const dated = date.getDate();
    
    return `${year}-${month}-${dated}`;
}
