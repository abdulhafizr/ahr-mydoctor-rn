import { showMessage } from "react-native-flash-message";
import { colors } from "../colors";

export const messageError = (message) => showMessage({
    message: message,
    type: 'default',
    backgroundColor: colors.red1,
    color: colors.white
})

export const messageSuccess = (message) => showMessage({
    message: message,
    type: 'default',
    backgroundColor: colors.secondary,
    color: colors.white
})
