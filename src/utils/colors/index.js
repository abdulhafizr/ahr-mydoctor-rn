const mainColors = {
	secondary: '#0BCAD4',
	primary: '#112340',
	gray1: '#7D8797',
	gray2: '#E9E9E9',
	gray3: '#EEEEEE',
	gray4: '#EDEEF0',
	deepGray: '#495A75',
	red1: '#E06379',
	white: '#fff',
	softTeal: '#EDFCFD',
	blue1: '#0066CB',
	lightteal: '#EDFCFD',
}

export const colors = {
	text: {
		primary: mainColors.primary,
		secondary: mainColors.gray1,
		white: mainColors.white
	},
	button: {
		primary: mainColors.secondary,
		secondary: mainColors.white,
		loading: mainColors.gray2,
	},
	...mainColors,
}