import { AsyncStorage } from 'react-native';
import { messageError } from '../showMessage';

export const storeData = async (key, value) => {
    try {
      await AsyncStorage.setItem(key, JSON.stringify(value))
    } catch (e) {
      messageError(e.message);
    }
}


export const getData = async (key) => {
    try {
      const value = await AsyncStorage.getItem(key)
      if(value !== null) {
        return JSON.parse(value);
      }
    } catch(e) {
      messageError(e.message);
    }
}
  
