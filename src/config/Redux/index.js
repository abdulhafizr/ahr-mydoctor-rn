import { createStore } from 'redux';

const initialState = {
    isLoading: false,
    defaultUser: 'https://firebasestorage.googleapis.com/v0/b/ahr-my-doctor.appspot.com/o/users%2Fuser_default_photo.png?alt=media&token=7d47fd96-64a4-4822-bf38-2af185af6a92',
}

const reduxer = (state = initialState, action) => {
    return state;
}

const store = createStore(reduxer);

export default store;