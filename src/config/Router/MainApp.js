import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Doctor, Hospitals, Messages } from '../../pages';
import { BottomNavigation } from '../../components';

const Tab = createBottomTabNavigator();

export default function MainApp() {
    return (
        <Tab.Navigator tabBar={(props) => <BottomNavigation {...props} /> }>
            <Tab.Screen name="Doctor" component={Doctor} />
            <Tab.Screen name="Messages" component={Messages} />
            <Tab.Screen name="Hospitals" component={Hospitals} />
        </Tab.Navigator>
    )
}
