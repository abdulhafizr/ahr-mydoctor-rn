import React from 'react';
import { GetStarted, Splash, Signup, Signin, UploadPhoto, ChooseDoctor, Chatting, UserProfile, UpdateUserProfile, DoctorProfile } from '../../pages';
import { createStackNavigator } from '@react-navigation/stack';
import MainApp from './MainApp';

const Stack = createStackNavigator();

const Router = () => {
    return (
      <Stack.Navigator initialRouteName="Splash">
      	<Stack.Screen 
      		name="Splash" 
      		component={Splash} 
      		options={{headerShown: false}}
      	/>
      	<Stack.Screen 
      		name="GetStarted" 
      		component={GetStarted} 
      		options={{headerShown: false}}
      	/>
      	<Stack.Screen 
      		name="Signup" 
      		component={Signup} 
      		options={{headerShown: false}}
      	/>
      	<Stack.Screen 
      		name="Signin" 
      		component={Signin} 
      		options={{headerShown: false}}
      	/>
		<Stack.Screen 
			name="UploadPhoto" 
			component={UploadPhoto} 
			options={{headerShown: false}}
      	/>
		<Stack.Screen 
			name="MainApp" 
			component={MainApp} 
			options={{headerShown: false}}
      	/>
		<Stack.Screen 
			name="ChooseDoctor" 
			component={ChooseDoctor} 
			options={{headerShown: false}}
      	/>
		<Stack.Screen 
			name="Chatting" 
			component={Chatting} 
			options={{headerShown: false}}
      	/>
		<Stack.Screen 
			name="UserProfile" 
			component={UserProfile} 
			options={{headerShown: false}}
      	/>
		<Stack.Screen 
			name="UpdateUserProfile" 
			component={UpdateUserProfile} 
			options={{headerShown: false}}
      	/>
		<Stack.Screen 
			name="DoctorProfile" 
			component={DoctorProfile} 
			options={{headerShown: false}}
      	/>
      </Stack.Navigator>
    );
}

export default Router;
