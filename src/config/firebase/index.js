import firebase from 'firebase';

firebase.initializeApp({
    apiKey: "AIzaSyCWGpWnYBTvPzK3RCG5-ao7lJnx9Xs7LHQ",
    authDomain: "ahr-my-doctor.firebaseapp.com",
    projectId: "ahr-my-doctor",
    storageBucket: "ahr-my-doctor.appspot.com",
    messagingSenderId: "22063776003",
    appId: "1:22063776003:web:fbe92793ecd4b05babe936",
})

export default firebase;
