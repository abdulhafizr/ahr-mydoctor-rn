import store from './Redux';
import Router from './Router';
import firebase from './firebase';

export { Router, firebase, store };
