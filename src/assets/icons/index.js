import ICArrowBackBlack from './ic_back_black.svg';
import ICArrowBackWhite from './ic_back_white.svg';
import ICAddBtnPhoto from './btn_add_photo.svg';
import ICRemoveBtnPhoto from './btn_remove_photo.svg';
import ICDoctor from './ic_doctor.svg';
import ICDoctorActive from './ic_doctor_active.svg';
import ICHospitals from './ic_hospitals.svg';
import ICHospitalsActive from './ic_hospitals_active.svg';
import ICMessages from './ic_messages.svg';
import ICMessagesActive from './ic_messages_active.svg';
import ICActive from './ic_active.svg';
import ICDoctorAnak from './ic_dokter_anak.svg';
import ICDoctorObat from './ic_dokter_obat.svg';
import ICDoctorPsikiater from './ic_dokter_psikiater.svg';
import ICDoctorUmum from './ic_dokter_umum.svg';
import ICStarRate from './ic_star_rate.svg';
import ICRightBlack from './ic_right_black.svg';
import ICBtnChatDark from './ic_btn_chat_dark.svg';
import ICBtnChatLight from './ic_btn_chat_light.svg';
import ICEditProfile from './ic_edit_profile.svg';
import ICHelpCenter from './ic_help_center.svg';
import ICLanguage from './ic_language.svg';
import ICRate from './ic_rate.svg';
import ICMale from './ic_male.svg';
import ICFemale from './ic_female.svg';

export { 
    ICArrowBackBlack, 
    ICArrowBackWhite, 
    ICAddBtnPhoto, 
    ICRemoveBtnPhoto,
    ICDoctor,
    ICDoctorActive,
    ICHospitals,
    ICHospitalsActive,
    ICMessages,
    ICMessagesActive,
    ICActive,
    ICDoctorAnak,
    ICDoctorObat,
    ICDoctorPsikiater,
    ICDoctorUmum,
    ICStarRate,
    ICRightBlack,
    ICBtnChatDark,
    ICBtnChatLight,
    ICEditProfile,
    ICHelpCenter,
    ICLanguage,
    ICRate,
    ICMale,
    ICFemale,
};
