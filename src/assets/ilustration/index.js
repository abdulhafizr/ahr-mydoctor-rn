import ILLogo from './Logo.svg';
import ILGetStarted from './GetStarted.png';
import ILUserDefaultPhoto from './user_default_photo.png';
import ILHospitalBg from './hospital_bg.png';

export { ILGetStarted, ILLogo, ILUserDefaultPhoto, ILHospitalBg };
